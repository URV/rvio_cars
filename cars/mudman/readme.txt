Car Information
================================================================
Car name : Mudman
Car Type : Repaint/Remap/Remodel
Top speed : 44.9 mph / 72.3 kph
Rating/Class : 5 (super-pro)
Installed folder : ...\cars\mudman
Description : Remap/Rehull/Repaint/etc of Freak Van (RVA original cars p.12, freak.zip on XTG). Models cleaned/fixed up a lot, sizes scaled down a bit, params mostly the same.

Blasting acceleration, 44mph top speed and SNW-tier grip would make this van freakishly overpowered in most cases. However the grip is also the main problem when combined with their large size and the soft suspension.
Rides up over obstacles and cars instead of bumping into them, easily spins out if traction gets unbalanced the wrong way.

Depending on the track and collision mode it can either dominate or be lap traffic fodder.

Update Log
================================================================
10 Nov 2020:
- Now super-pro, it was OP in pros anyways
- Parameters tweaked for balance within super-pro class
- New texture shading (hat-tip to Shara)
- Wheels and springs re-worked and -textured
~ Full texture re-work is planned

Author Information
================================================================
Author Name : Raster Gotolei
Email Address : rgotolei@gmail.com

Construction
================================================================
Base : Freak Van by Megalon
Editor(s) used : blender for remapping, gimp for skin/carbox, inkscape for wheels and some details

Additional Credits
================================================================
Megalon for original car
Jigebren for Blender plugin
Marv for newer Blender plugin
Saffron for balancing work
Shara for texture shading

Copyright / Permissions
================================================================
You may do whatever you want with this car.
XCFs/SVGs and a bunch of other junk in RVL thread download.
