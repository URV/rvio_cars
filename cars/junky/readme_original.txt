Readme file of Dump Truck.
Updated at: October 25, 2018

==============================
========== Car Info ==========
==============================

Name:Dump Truck
Author: Allan1
Creation date: April 09, 2013
RVZ car type: Remodel
Folder name: junkguy
Rating: Rookie
Class: Electric
Drivetrain: Four-wheel Drive
Top speed: 31 mph
Body mass: 2.3 kg


==============================
=========== Setup ============
==============================

Extract the zipped file into your Re-Volt directory. Files will be automatically placed on the correct folders.

There's an alternative blue/white skin inside. To use it (if you're not using RVGL), just open parameters.txt and change the TPAGE line to "cars\dumptruck\caror.bmp"



==============================
======== Description =========
==============================

That's a truck based on RC Revenge's Big Rock and the truck model from Toy World levels. It's a low powered rookie just like Harvester. Springs are quite bouncy.


==============================
========== Credits ===========
==============================

RC Revenge's Big Rock and the truck model from Toy World were used as base (as mentioned above). Both owned by Acclaim.

Tracks in the previews are Arena Extreme by Bak95, Botanical Garden and Ghost Town 1.

Tools:
Autodesk 3D Studio Max
Asetools (By Ali)
Adobe Photoshop CS2
Microsoft Paint
RVGL (By Huki, Marv and Jig)
Inkscape
Prm2Hul (By Jig)
Zmodeler
Microsoft Notepad


==============================
========= Changelog ==========
==============================

October 30, 2015:
-Changed name
-Fixed body's alignment
-Fixed aerial's offset
-Added an alternative skin
-Decreased top speed (moved to Rookies)
-Added axles

November 25, 2016:
-Increased top speed to 31 mph (28 before)

August 10, 2017
-Changed body AngRes
-Fixed axles


==============================
=== Permissions and Terms ====
==============================

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner (preferable through a readme file).

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.


==============================
======== Final Notes =========
==============================

Please contact me if you find bugs or if you have any suggestion or idea to improve this creation. You can do so at the comments section of Re-Volt Zone, or even with an email or private message at the forum Re-Volt Hideout, or even Discord.

My email address: allanmoraes27@yahoo.com

Re-Volt Hideout profile: https://forum.re-volt.io/memberlist.php?mode=viewprofile&u=73

My Discord tag: Allan#5269



For more stuff like downloads and wips, visit my website: http://allanmitestainer.wix.com/home


Have a nice race!

Allan