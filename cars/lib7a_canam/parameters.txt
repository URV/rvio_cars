{

;============================================================
;============================================================
; Abandoned Design Project PLUS - by burner94, idea by FrenchFry, help by Norfair
;============================================================
;============================================================
Name      	"Proto Combo"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\lib7a_canam\body.prm"
MODEL 	1 	"cars\lib7a_canam\wheel-l.prm"
MODEL 	2 	"cars\lib7a_canam\wheel-r.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"cars\lib7a_canam\axle.prm"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\misc\AerialT.m"
TPAGE 	"cars\lib7a_canam\car.bmp"
TCARBOX	"cars\lib7a_canam\carbox.bmp"
TSHADOW "cars\lib7a_canam\shadow.bmp"
;)SHADOWTABLE -59.9 59.9 70.0 -80.4 -5.6
COLL 	"cars\lib7a_canam\hull.hul"
EnvRGB 	200 200 200

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	4 			; Skill level (rookie, amateur, ...)
TopEnd     	3695.610596 			; Actual top speed (mph) for frontend bars
Acc        	4.715998 			; Acceleration rating (empirical)
Weight     	1.400000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	0 			; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)
;)Statistics	TRUE

;====================
; Handling related stuff
;====================

SteerRate  	5.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	40.000000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 -3.000000 0.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0 		; Calculated in game
Mass       	1.400000
Inertia    	1000.000000 0.000000 0.000000
           	0.000000 1600.000000 0.000000
           	0.000000 0.000000 600.000000
Gravity		2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	25.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-21.000000 3.000000 32.000000
Offset2  	-6.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.250000
EngineRatio 	20000.000000
Radius      	12.000000
Mass        	0.400000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	12.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.017000
StaticFriction  	1.900000
KineticFriction 	1.750000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	21.000000 3.000000 32.000000
Offset2  	6.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.250000
EngineRatio 	20000.000000
Radius      	12.000000
Mass        	0.400000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	12.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.017000
StaticFriction  	1.900000
KineticFriction 	1.750000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-26.000000 3.000000 -32.000000
Offset2  	-6.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.030000
EngineRatio 	20000.000000
Radius      	12.000000
Mass        	0.400000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	12.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.017000
StaticFriction  	1.900000
KineticFriction 	1.750000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	26.000000 3.000000 -32.000000
Offset2  	6.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.030000
EngineRatio 	20000.000000
Radius      	12.000000
Mass        	0.400000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	12.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.017000
StaticFriction  	1.900000
KineticFriction 	1.750000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	450.000000
Damping     	9.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	450.000000
Damping     	9.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	450.000000
Damping     	9.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	450.000000
Damping     	9.000000
Restitution 	-0.950000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	9
Offset      	-3.000000 -5.000000 32.000000
Length      	28.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	9
Offset      	3.000000 -5.000000 32.000000
Length      	28.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	9
Offset      	-3.000000 -5.000000 -32.000000
Length      	28.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	9
Offset      	3.000000 -5.000000 -32.000000
Length      	28.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	1.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	15.000000 -12.000000 30.000000
Direction   	0.000000 -1.000000 0.000000
Length      	20.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	82.150002
UnderRange  	3162.633545
UnderFront	 	251.520004
UnderRear   	1125.232666
UnderMax    	0.271073
OverThresh  	1704.070923
OverRange   	1391.000000
OverMax     	0.746230
OverAccThresh  	113.930000
OverAccRange   	2574.388916
PickupBias     	16383
BlockBias      	16383
OvertakeBias   	16383
Suspension     	9830
Aggression     	0
}           	; End AI

}

2D412F2B