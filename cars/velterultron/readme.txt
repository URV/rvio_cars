Information
-----------------
Car Name:	Vector Ultron
Rating:		Pro
Top speed:	44.4 mph
Acceleration:	3.20 m/s�
Weight:		1.9 kg
Author:		Saffron


Description
-----------------
An old project coming to life after spending nearly two years in limbo.

Based off of the Vector W8, it has high top speed and good acceleration for the class, with its only downside being its quirky handling and recovery.

Expect it to be a top contender in the right hands on less technical tracks.


Requirements
-----------------
You MUST use the latest RVGL patch for the textures to display properly.


Credits
-----------------
Polyphony Digital for their Vector W8 model as a reference
The Blender Foundation for Blender
Marv, Martin and Huki for the Blender plug-in
Jigebren for PRM2HUL
Mighty for the wheel texture
The RVGL Team for RVGL
Rick Brewster for Paint.NET


Permission
-----------------
Make sure to credit me and Polyphony Digital for whatever you end up doing with or based off this car.