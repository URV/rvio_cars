{

;============================================================
;============================================================
; Blobster
;============================================================
;============================================================
Name       	"Blobster"


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	true
Selectable 	true
;)CPUSelectable	true
;)Statistics 	true
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	0 			; Skill level (rookie, amateur, ...)
TopEnd     	2932.551270 		; Actual top speed (mph) for frontend bars
Acc        	5.916147 		; Acceleration rating (empirical)
Weight     	1.800000 		; Scaled weight (for frontend bars)
Trans      	2 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.500000 		; Max Revs (for rev counter, deprecated...)

;====================
; Model Filenames
;====================

MODEL 	0 	"cars/lib4a_blobster/body.prm"
MODEL 	1 	"cars/lib4a_blobster/wheelfl.prm"
MODEL 	2 	"cars/lib4a_blobster/wheelfr.prm"
MODEL 	3 	"cars/lib4a_blobster/wheelbl.prm"
MODEL 	4 	"cars/lib4a_blobster/wheelbr.prm"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"cars/lib4a_blobster/faxle.prm"
MODEL 	9 	"cars/lib4a_blobster/baxle.prm"
MODEL 	10 	"cars/lib4a_blobster/spring.prm"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/AerialT.m"
COLL 		"cars/lib4a_blobster/hull.hul"
TPAGE 		"cars/lib4a_blobster/car.bmp"
;)TCARBOX 	"cars/lib4a_blobster/carbox.bmp" 			; Carbox texture
;)TSHADOW 	"cars/lib4a_blobster/shadow.bmp" 			; Shadow texture
;)SHADOWINDEX 	-1 							; Use a default shadow (0 to 27 or -1)
;)SHADOWTABLE 	68.000000 -68.000000 77.000000 -65.000000 -5.000000 	; Left, right, front, back, height (relative to model center)
;)SFXENGINE 	"NONE"
;)SFXSERVO 	"NONE"
;)SFXHONK 	"NONE"
EnvRGB 		50 50 50

;====================
; Handling related stuff
;====================

SteerRate  	4.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	6.000000 			; Rate at which Engine voltage approaches set value
TopSpeed   	28.200000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 5.000000 -4.000000 	; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 	; Weapon generation offset (deprecated)
;)WeaponOffset1	0.000000 -32.000000 64.000000 	; Offset for shockwaves and fireworks
;)WeaponOffset2	0.000000 -16.000000 80.000000 	; Offset for water bombs
;)Flippable	false 		; Rotor car effect
;)Flying   	false 		; Flying like the UFO car
;)ClothFx  	false 		; Mystery car cloth effect

;====================
; Camera details
;====================

;)CAMATTACHED {	; Start Camera
;)HoodOffset   	0.000000 0.000000 0.000000 	; Offset from model center
;)HoodLook     	0.050000 			; Look angle (-0.25 to 0.25, 0.0 - straight ahead)
;)RearOffset   	0.000000 0.000000 0.000000
;)RearLook     	0.050000
;)FixedOffset  	true 				; Is offset fixed or moving
;)FixedLook    	true 				; Is look fixed or moving
;)UseDefault   	true 				; Use default offsets (computed in game)
;)}            	; End Camera

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0.000000 -8.000000 0.000000
Mass       	1.800000
Inertia    	2000.000000 0.000000 0.000000
           	0.000000 1800.000000 0.000000
           	0.000000 0.000000 1110.000000
Gravity    	2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 		; Linear air resistance
AngRes     	0.001000 		; Angular air resistance
ResMod     	20.000000 		; AngRes scale when in air
Grip       	0.010000 		; Converts downforce to friction value
StaticFriction 	0.800000
KineticFriction 0.500000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	2
Offset1  	-26.000000 -7.000000 57.000000
Offset2  	-8.500000 0.000000 0.000000
IsPresent   	true
IsPowered   	false
IsTurnable  	true
SteerRatio  	-0.500000
EngineRatio 	15000.000000
Radius      	17.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	25.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.050000
Grip            0.026000
StaticFriction  1.650000
KineticFriction 1.400000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	1
Offset1  	26.000000 -7.000000 57.000000
Offset2  	8.500000 0.000000 0.000000
IsPresent   	true
IsPowered   	false
IsTurnable  	true
SteerRatio  	-0.500000
EngineRatio 	15000.000000
Radius      	17.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	25.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.050000
Grip            0.026000
StaticFriction  1.650000
KineticFriction 1.400000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	4
Offset1  	-25.500000 -9.000000 -38.000000
Offset2  	-14.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	1.000000
EngineRatio 	48000.000000
Radius      	20.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	30.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.080000
Grip            0.080000
StaticFriction  1.550000
KineticFriction 1.400000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	3
Offset1  	25.500000 -9.000000 -38.000000
Offset2  	14.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	1.000000
EngineRatio 	48000.000000
Radius      	20.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	30.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    0.080000
Grip            0.080000
StaticFriction  1.550000
KineticFriction 1.400000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	10
Offset      	-20.000000 -38.000000 45.000000
Length      	20.000000
Stiffness   	350.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	10
Offset      	20.000000 -38.000000 45.000000
Length      	20.000000
Stiffness   	350.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	10
Offset      	-20.000000 -44.000000 0.000000
Length      	20.000000
Stiffness   	350.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	10
Offset      	20.000000 -44.000000 0.000000
Length      	20.000000
Stiffness   	350.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	8
Offset      	0.000000 -12.000000 57.000000
Length      	20.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	8
Offset      	0.000000 -12.000000 57.000000
Length      	20.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	9
Offset      	0.000000 -15.000000 -38.000000
Length      	15.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	9
Offset      	0.000000 -15.000000 -38.000000
Length      	15.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
;)Type      	1 				; 1: Default rot, 2: Turn with steer, 4: Translate with speed, 6: 2 and 4
;)Trans     	0.000000 3.000000 6.000000 	; Translation max
;)TransVel  	0.001000 			; Velocity factor
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	-12.500000 -43.000000 -44.000000
Direction   	0.000000 -1.000000 0.000000
Length      	14.000000
Stiffness   	7500.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	; Start AI
UnderThresh 	150.000000
UnderRange  	3907.274902
UnderFront  	3007.185547
UnderRear   	335.000000
UnderMax    	0.782781
OverThresh  	546.844604
OverRange   	1391.000000
OverMax     	0.730000
OverAccThresh  	10.000000
OverAccRange   	1448.683105
PickupBias     	29490
BlockBias      	16383
OvertakeBias   	16383
Suspension     	29490
Aggression     	0
}           	; End AI

}

