made by BloodBTF, July 2020

Car: Winger
Class: Special (Jet powered)
Rating: Semi-Pro
Trans: 4WD
Top Speed: 38MPH
Accel: good
Handling: great
Mass: 2.0kg


Winger isnt the average offroad truck. For one, it's source of propulsion comes from two jet thrusters strapped to the back of it. Those Jets give you the advantage of a great top speed, but at the cost of mediocre acceleration. Being a short course truck, winger naturally has great response and offroad handling. However, because of it's tall ride height, you can find yourself tipping over if you hit a wall or land a jump at an awkward angle.

Special thanks to Norfair for the wheel texture, FrenchFry for feedback, and Hi-Ban for the original Traxxas Slash model. 

tools used: RVGL, Blender, GIMP

Engine sound source from the "Jet Thruster" sound made by Unity Technologies, as part of the standard assets pack


you have my permission to do whatever you want with this car, as long as it abides by the following rules:
1. you must credit all appropriate authors
2. you may not use this work for commercial purposes