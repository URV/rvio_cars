
  S U N N Y B O Y
  
  Version 1.0 from March 6th, 2022

================================================================
  Car information
================================================================

Date      : 06/03/2022
Car Name  : Sunnyboy
Author    : Kiwi, Skarma
Car Type  : Remodel
Folder    : ...\cars\sunnyboy
Top Speed : 57 kph
Mass      : 2,2 KG
Rating    : 3 (Advanced)

Sunnyboy is an Advanced rated beach buggy based on Stonemason
by RV_Pure and RV_Passion. The parameters were done by Skarma.

Before getting a standalone car, I released Sunnyboy as a skin
for Stonemason already. Since this day I dreamt of a standalone 
release - and here it is! Thank you RV_Pure for your approval.

The car can be a bit unstable and inconsistent at times,
especially when using a battery. Other than that it's a nice
little ride - have fun!


================================================================
  Construction
================================================================

Polygons       : 896 (Body, Axles, Shocks, Wheels)

Base           : Stonemason by RV_Pure & RV_Passion

Editors used   : Blender 2.79b
                 Ulead Photo Impact 12
                 Notepad++


================================================================
  Thank You
================================================================

+ RV_Pure and RV_Passion for the base car
+ RV_Pure for the approval to release this as a standalone
+ Skarma for the parameters


================================================================
  Copyright / Permissions
================================================================

Please do not change any parts of this car without asking, as
long it's not for your personal use. You can reuse any parts of
this car for your own car, as long you mention the original
creators accordingly.
