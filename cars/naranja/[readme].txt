Car information
================================================================
Car name: Naranja Turbo
Car Type: Repaint
Top speed: 47 kph
Rating/Class: 0 (Rookie)
Installed folder: ...\cars\naranja
Description: 

This is Naranja Turbo, a repaint of Xarc's Perfect Shot.
The parameters are equal to Perfect Shot.

The download also includes the original Perfect Shot livery.

Have fun! :)
-Kiwi

Author Information
================================================================
Model: Xarc
Texture: Kiwi
Parameters: Xarc
 
Construction
================================================================
Editor(s) used: Notepad++, PhotoImpact12
 

Copyright / Permissions
================================================================
Please do not change any parts of this car without asking, as long it's not for your personal use.
You can reuse any parts of this car for your own car, as long you mention the
original creators accordingly.


Version 1.2 from April 29th, 2022
Thanks to LivingWithGames for fixing the wheel texture.